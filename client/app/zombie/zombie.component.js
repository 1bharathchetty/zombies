import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './zombie.routes';

export class ZombieController {
  $http;
  message;
  messageJSON;
  username;
  greetingStatus;
  errorMessage;

  /*@ngInject*/
  constructor($http) {
    this.$http = $http;
  }

  $onInit() {
    this.$http.get('/api/zombies')
      .then(response => {
        this.message = response.data;
      });

    this.$http.get('/api/zombies/hello')
      .then(response => {
        this.messageJSON = response.data;
      });
  }

  sendGreeting() {
    if(this.username) {
      this.$http.post('/api/zombies/hello', {
        username: this.username,
        isGuest : true,
        status : 'unknown'
      }).then(response => {
        this.greetingStatus = response.data;
      }).catch(err => {
        this.errorMessage = err.data;
      })
    }
  }
  //
  // deleteThing(thing) {
  //   this.$http.delete(`/api/things/${thing._id}`);
  // }
}

export default angular.module('zombiesApp.zombie', [uiRouter])
  .config(routing)
  .component('zombie', {
    template: require('./zombie.html'),
    controller: ZombieController
  })
  .name;
