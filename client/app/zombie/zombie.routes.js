'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('zombie', {
    url: '/zombies',
    template: '<zombie></zombie>'
  });
}
