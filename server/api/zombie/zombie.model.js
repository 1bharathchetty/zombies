'use strict';

import mongoose from 'mongoose';

var ZombieSchema = new mongoose.Schema({
  name: String,
  country: String,
  status: String
});

export default mongoose.model('Zombie', ZombieSchema);
