'use strict';
import * as auth from '../../auth/auth.service';

var express = require('express');
var controller = require('./zombie.controller');

var router = express.Router();

router.get('/', controller.greetUser);

router.get('/hello', controller.greet);

router.post('/hello', auth.isAuthenticated(), controller.postGreeting);

// router.get('/', controller.index);
// router.get('/:id', controller.show);
router.post('/', controller.create);
// router.put('/:id', controller.upsert);
// router.patch('/:id', controller.patch);
// router.delete('/:id', controller.destroy);

module.exports = router;
